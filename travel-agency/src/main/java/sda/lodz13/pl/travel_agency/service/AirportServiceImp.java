package sda.lodz13.pl.travel_agency.service;

import sda.lodz13.pl.travel_agency.model.Airport;

import java.util.List;

public interface AirportServiceImp {
    Airport addAirport(Airport airport);

    Airport getAirportById(long id);

    List<Airport> getAirportByName(String name);

    Airport modifyAirport(Airport airport);

    boolean deleteAirport(long id);

    List<Airport> getListAllAirport();
}

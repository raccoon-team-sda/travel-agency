package sda.lodz13.pl.travel_agency.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.controller.model.TripRequest;
import sda.lodz13.pl.travel_agency.model.Airport;
import sda.lodz13.pl.travel_agency.model.City;
import sda.lodz13.pl.travel_agency.model.Hotel;
import sda.lodz13.pl.travel_agency.model.Trip;
import sda.lodz13.pl.travel_agency.service.AirportService;
import sda.lodz13.pl.travel_agency.service.CityService;
import sda.lodz13.pl.travel_agency.service.HotelService;
import sda.lodz13.pl.travel_agency.service.TripService;

import java.util.List;

@RestController
@RequestMapping("rest/trip")
public class TripRestController {

    private final TripService tripService;
    private final AirportService airportService;
    private final HotelService hotelService;
    private final CityService cityService;

    public TripRestController(TripService tripService, AirportService airportService, HotelService hotelService, CityService cityService) {
        this.tripService = tripService;
        this.airportService = airportService;
        this.hotelService = hotelService;
        this.cityService = cityService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Trip> saveTrip(@RequestBody TripRequest tripRequest) {
        Trip t = new Trip();
        t.setId(tripRequest.getId());

        Airport airportFromId = airportService.getAirportById(tripRequest.getAirportFrom());
        t.setAirportFrom(airportFromId);

        Airport airportToId = airportService.getAirportById(tripRequest.getAirportTo());
        t.setAirportTo(airportToId);

        Hotel hotelToId = hotelService.getHotelById(tripRequest.getHotelTo());
        t.setHotelTo(hotelToId);

        City cityToId = cityService.getCityById(tripRequest.getCityTo());
        t.setCityTo(cityToId);

        t.setDepartureDate(tripRequest.getDepartureDate());
        t.setReturnDate(tripRequest.getReturnDate());
        t.setCountOfDays(tripRequest.getReturnDate() - tripRequest.getDepartureDate());
        t.setType(tripRequest.getType());
        t.setPriceForAdult(tripRequest.getPriceForAdult());
        t.setPriceForChild(tripRequest.getPriceForChild());
        t.setPromotion(tripRequest.getPromotion());
        t.setCountOfPerson(tripRequest.getCountOfPerson());
        t.setDescription(tripRequest.getDescription());

        Trip addedTrip = tripService.addTrip(t);

        return ResponseEntity.ok(addedTrip);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<Trip>> getAll() {
        List<Trip> listAllTrips = tripService.getListAllTrips();
        return ResponseEntity.ok(listAllTrips);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<TripRequest> getTripById(@PathVariable Long id) {
        TripRequest tripRequest = new TripRequest();
        Trip trip = tripService.getTripById(id);

        tripRequest.setId(trip.getId());
        tripRequest.setAirportFrom(trip.getAirportFrom().getId());
        tripRequest.setAirportTo(trip.getAirportTo().getId());
        tripRequest.setHotelTo(trip.getHotelTo().getId());
        tripRequest.setCityTo(trip.getCityTo().getId());
        tripRequest.setDepartureDate(trip.getDepartureDate());
        tripRequest.setReturnDate(trip.getReturnDate());
        tripRequest.setCountOfDays(trip.getCountOfDays());
        tripRequest.setType(trip.getType());
        tripRequest.setPriceForAdult(trip.getPriceForAdult());
        tripRequest.setPriceForChild(trip.getPriceForChild());
        tripRequest.setPromotion(trip.getPromotion());
        tripRequest.setCountOfPerson(trip.getCountOfPerson());
        tripRequest.setDescription(trip.getDescription());

        try {
            return ResponseEntity.ok(tripRequest);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping(value = "/update")
    public ResponseEntity<Trip> modifyTrip(@RequestBody TripRequest tripRequest) {
        Trip t = new Trip();
        t.setId(tripRequest.getId());

        Airport airportFromId = airportService.getAirportById(tripRequest.getAirportFrom());
        t.setAirportFrom(airportFromId);

        Airport airportToId = airportService.getAirportById(tripRequest.getAirportTo());
        t.setAirportTo(airportToId);

        Hotel hotelToId = hotelService.getHotelById(tripRequest.getHotelTo());
        t.setHotelTo(hotelToId);

        City cityToId = cityService.getCityById(tripRequest.getCityTo());
        t.setCityTo(cityToId);

        t.setDepartureDate(tripRequest.getDepartureDate());
        t.setReturnDate(tripRequest.getReturnDate());
        t.setCountOfDays(tripRequest.getReturnDate() - tripRequest.getDepartureDate());
        t.setType(tripRequest.getType());
        t.setPriceForAdult(tripRequest.getPriceForAdult());
        t.setPriceForChild(tripRequest.getPriceForChild());
        t.setPromotion(tripRequest.getPromotion());
        t.setCountOfPerson(tripRequest.getCountOfPerson());
        t.setDescription(tripRequest.getDescription());

        Trip modifyTrip = tripService.modifyTrip(t);
        return ResponseEntity.ok(modifyTrip);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<Trip> deleteTrip(@PathVariable Long id) {
        boolean isDelete = tripService.deleteTrip(id);
        return isDelete ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }


}

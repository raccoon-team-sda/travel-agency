package sda.lodz13.pl.travel_agency.service;

import sda.lodz13.pl.travel_agency.model.Trip;

import java.util.List;

public interface TripServiceImp {
    List<Trip> getListAllTrips();

    Trip addTrip(Trip trip);

    Trip getTripById(long id);

    List<Trip> getTripByName(String name);

    Trip modifyTrip(Trip trip);

    boolean deleteTrip(long id);

}

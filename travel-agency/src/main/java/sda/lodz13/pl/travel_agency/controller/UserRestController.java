package sda.lodz13.pl.travel_agency.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.model.User;
import sda.lodz13.pl.travel_agency.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/rest/user")
public class UserRestController {

    @Autowired
    private final UserService userService;

    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<User> saveUser(@RequestBody User user) {
        User newUser = userService.addUser(user);
        return ResponseEntity.ok(newUser);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<User>> getAll() {
        List<User> listAllUsers = userService.getListAllUser();
        return ResponseEntity.ok(listAllUsers);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<User> getUserById(@PathVariable Long id) {
        try {
            User userByID = userService.getUserById(id);
            return ResponseEntity.ok(userByID);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping(value = "/update")
    public ResponseEntity<User> modifyUser(@RequestBody User user) {
        User userModify = userService.modifyUser(user);
        return ResponseEntity.ok(userModify);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        boolean isDelete = userService.deleteUser(id);
        return isDelete ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }

}

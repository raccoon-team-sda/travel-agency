package sda.lodz13.pl.travel_agency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.controller.model.CountryRequest;
import sda.lodz13.pl.travel_agency.model.Continent;
import sda.lodz13.pl.travel_agency.service.ContinentService;

import java.util.List;


@RestController
@RequestMapping("/rest/continent")
public class ContinentRestController {

    @Autowired
    private final ContinentService continentService;

    public ContinentRestController(ContinentService continentService) {
        this.continentService = continentService;
    }


    @RequestMapping(method = RequestMethod.GET, value = "/getExample")
    @ResponseBody
    public String purchase() {
        return "example";
    }

    @CrossOrigin(origins = "http://localhost:8086")
//    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Continent> saveContinent(@RequestBody Continent continent) {
        Continent continentAdded = continentService.addContinent(continent);
//        Continent continentAdded = continentService.addContinent(continent);
        return ResponseEntity.ok(continentAdded);
    }

    @CrossOrigin(origins = "http://localhost:8086")
//        @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<Continent>> getAll() {
        List<Continent> listAllContinents = continentService.getListAllContinents();
        return ResponseEntity.ok(listAllContinents);
    }

    @CrossOrigin(origins = "http://localhost:8086")
//    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<Continent> getContById(@PathVariable Long id) {

        try {
            Continent continentById = continentService.getContinentById(id);
            return ResponseEntity.ok(continentById);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "http://localhost:8086")
//    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = "/findByName")
    public ResponseEntity<List<Continent>> findContByName(@RequestParam String name) {
        List<Continent> continentByName = continentService.getContinentByName(name);
        return ResponseEntity.ok(continentByName);
    }

//    @CrossOrigin(origins = "http://localhost:4200")
    @CrossOrigin(origins = "http://localhost:8086")
    @PutMapping(value = "/update")
    public ResponseEntity<Continent> modifyCont(@RequestBody Continent continent) {
        Continent continentModify = continentService.modifyContinent(continent);
        return ResponseEntity.ok(continentModify);
    }

//    @CrossOrigin(origins = "http://localhost:4200")
    @CrossOrigin(origins = "http://localhost:8086")
    @DeleteMapping(value = "/delete/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id) {
        boolean isDelete = continentService.deleteContinent(id);
        return isDelete ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }
}


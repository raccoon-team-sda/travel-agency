package sda.lodz13.pl.travel_agency.controller.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TripRequest {
    Long id;
    long airportFrom;
    long airportTo;
    long hotelTo;
    long cityTo;
    int departureDate;
    int returnDate;
    int countOfDays;
    String type;
    double priceForAdult;
    double priceForChild;
    String promotion;
    int countOfPerson;
    String description;

}

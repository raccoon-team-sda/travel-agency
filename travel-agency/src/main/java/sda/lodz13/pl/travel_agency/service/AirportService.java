package sda.lodz13.pl.travel_agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.lodz13.pl.travel_agency.model.Airport;
import sda.lodz13.pl.travel_agency.repository.AirportRepository;

import java.util.List;

@Service
public class AirportService implements AirportServiceImp {

    @Autowired
    private AirportRepository airportRepository;

    @Override
    public Airport addAirport(Airport airport) {
        return airportRepository.save(airport);
    }

    @Override
    public Airport getAirportById(long id) {
        return airportRepository.findById(id).get();
    }

    @Override
    public List<Airport> getAirportByName(String name) {
        return airportRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public Airport modifyAirport(Airport airport) {
        return airportRepository.save(airport);
    }

    @Override
    public boolean deleteAirport(long id) {
        try {
            airportRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<Airport> getListAllAirport() {
        return airportRepository.findAll();
    }
}

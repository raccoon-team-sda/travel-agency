package sda.lodz13.pl.travel_agency.service;

import sda.lodz13.pl.travel_agency.model.Continent;
import sda.lodz13.pl.travel_agency.model.Country;

import java.util.List;

public interface CountryServiceImp {

    Country addCountry(Country country);

    Country getCountryById(long id);

    List<Country> getCountryByName(String name);

    Country modifyCountry(Country country);

    boolean deleteCountry(long id);

    List<Country> getListAllCountry();
}

package sda.lodz13.pl.travel_agency.controller.model;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AirportRequest {
    Long id;
    String name;
    long cityId;
}

package sda.lodz13.pl.travel_agency.controller.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryRequest {
    Long id;
    String name;
    long continentId;
}

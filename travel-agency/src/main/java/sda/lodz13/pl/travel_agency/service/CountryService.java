package sda.lodz13.pl.travel_agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.lodz13.pl.travel_agency.model.Country;
import sda.lodz13.pl.travel_agency.repository.CountryRepository;

import java.util.List;

@Service
public class CountryService implements CountryServiceImp {

    @Autowired
    private CountryRepository countryRepository;

    @Override
    public Country addCountry(Country country) {
        return countryRepository.save(country);
    }

    @Override
    public Country getCountryById(long id) {
        return countryRepository.findById(id).get();
    }

    @Override
    public List<Country> getCountryByName(String name) {
        return countryRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public Country modifyCountry(Country country) {
        return countryRepository.save(country);
    }

    @Override
    public boolean deleteCountry(long id) {
        try {
            countryRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<Country> getListAllCountry() {
        return countryRepository.findAll();
    }
}

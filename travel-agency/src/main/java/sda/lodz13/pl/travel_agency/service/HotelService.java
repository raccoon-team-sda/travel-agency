package sda.lodz13.pl.travel_agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.lodz13.pl.travel_agency.model.Hotel;
import sda.lodz13.pl.travel_agency.repository.HotelRepository;

import java.util.List;

@Service
public class HotelService implements HotelServiceImp {

    @Autowired
    private HotelRepository hotelRepository;

    @Override
    public List<Hotel> getListAllHotel() {
        return hotelRepository.findAll();
    }

    @Override
    public Hotel addHotel(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    @Override
    public Hotel getHotelById(long id) {
        return hotelRepository.findById(id).get();
    }

    @Override
    public List<Hotel> getHotelByName(String name) {
        return hotelRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public Hotel modifyAirport(Hotel hotel) {
        return hotelRepository.save(hotel);
    }

    @Override
    public boolean deleteHotel(long id) {
        try {
            hotelRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}

package sda.lodz13.pl.travel_agency.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.controller.model.CityReqest;
import sda.lodz13.pl.travel_agency.model.City;
import sda.lodz13.pl.travel_agency.model.Country;
import sda.lodz13.pl.travel_agency.service.CityService;
import sda.lodz13.pl.travel_agency.service.CountryService;

import java.util.List;

@RestController
@RequestMapping("rest/city")
public class CityRestController {

    @Autowired
    private final CityService cityService;
    private final CountryService countryService;

    public CityRestController(CityService cityService, CountryService countryService) {
        this.cityService = cityService;
        this.countryService = countryService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<City> saveCity(@RequestBody CityReqest cityReqest) {
        City c = new City();

        c.setId(cityReqest.getId());
        c.setName(cityReqest.getName());

        Country country = countryService.getCountryById(cityReqest.getCountryId());
        c.setCountryId(country);

        City cityAdded = cityService.addCity(c);
        return ResponseEntity.ok(cityAdded);

    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<City>> getAll() {
        List<City> listAllCities = cityService.getListAllCities();
        return ResponseEntity.ok(listAllCities);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<CityReqest> getCityById(@PathVariable Long id) {
        CityReqest cityReqest = new CityReqest();
        City city = cityService.getCityById(id);

        cityReqest.setId(city.getId());
        cityReqest.setName(city.getName());
        cityReqest.setCountryId(city.getCountryId().getId());

        try {
            return ResponseEntity.ok(cityReqest);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping(value = "/update")
    public ResponseEntity<City> modifyCity(@RequestBody CityReqest cityReqest) {
        City c = new City();

        c.setId(cityReqest.getId());
        c.setName(cityReqest.getName());

        Country country = countryService.getCountryById(cityReqest.getCountryId());
        c.setCountryId(country);

        City cityAdded = cityService.modifyCity(c);
        return ResponseEntity.ok(cityAdded);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<City> deleteCity(@PathVariable Long id) {
        boolean isDelete = cityService.deleteCity(id);
        return isDelete ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();


    }
}

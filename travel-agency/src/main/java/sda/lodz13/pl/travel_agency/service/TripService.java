package sda.lodz13.pl.travel_agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.lodz13.pl.travel_agency.model.Trip;
import sda.lodz13.pl.travel_agency.repository.TripRepository;

import java.util.List;

@Service
public class TripService implements TripServiceImp {

    @Autowired
    private TripRepository tripRepository;

    @Override
    public List<Trip> getListAllTrips() {
        return tripRepository.findAll();
    }

    @Override
    public Trip addTrip(Trip trip) {
        return tripRepository.save(trip);
    }

    @Override
    public Trip getTripById(long id) {
        return tripRepository.findById(id).get();
    }

    @Override
    public List<Trip> getTripByName(String name) {
        return null;
    }

    @Override
    public Trip modifyTrip(Trip trip) {
        return tripRepository.save(trip);
    }

    @Override
    public boolean deleteTrip(long id) {

        try {
            tripRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}
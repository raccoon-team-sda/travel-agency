package sda.lodz13.pl.travel_agency.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.controller.model.HotelRequest;
import sda.lodz13.pl.travel_agency.model.City;
import sda.lodz13.pl.travel_agency.model.Hotel;
import sda.lodz13.pl.travel_agency.service.CityService;
import sda.lodz13.pl.travel_agency.service.HotelService;

import java.util.List;

@RestController
@RequestMapping("/rest/hotel")
public class HotelRestController {

    private final HotelService hotelService;
    private final CityService cityService;

    public HotelRestController(HotelService hotelService, CityService cityService) {
        this.hotelService = hotelService;
        this.cityService = cityService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Hotel> saveAirport(@RequestBody HotelRequest hotelRequest) {
        Hotel hotel = new Hotel();

        hotel.setId(hotelRequest.getId());
        hotel.setName(hotelRequest.getName());
        hotel.setStandard(hotelRequest.getStandard());
        hotel.setDescription(hotelRequest.getDescription());

        City cityId = cityService.getCityById(hotelRequest.getCityId());
        hotel.setCityId(cityId);

        Hotel addedHotel = hotelService.addHotel(hotel);
        return ResponseEntity.ok(addedHotel);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<Hotel>> getAll() {
        List<Hotel> listAllHotels = hotelService.getListAllHotel();
        return ResponseEntity.ok(listAllHotels);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<HotelRequest> getHotelById(@PathVariable Long id) {
        HotelRequest hotelRequest = new HotelRequest();
        Hotel hotel = hotelService.getHotelById(id);

        hotelRequest.setId(hotel.getId());
        hotelRequest.setName(hotel.getName());
        hotelRequest.setStandard(hotel.getStandard());
        hotelRequest.setDescription(hotel.getDescription());
        hotelRequest.setCityId(hotel.getCityId().getId());

        try {
            return ResponseEntity.ok(hotelRequest);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping(value = "/update")
    public ResponseEntity<Hotel> modifyHotel(@RequestBody HotelRequest hotelRequest) {
        Hotel hotel = new Hotel();

        hotel.setId(hotelRequest.getId());
        hotel.setName(hotelRequest.getName());
        hotel.setStandard(hotelRequest.getStandard());
        hotel.setDescription(hotelRequest.getDescription());

        City cityId = cityService.getCityById(hotelRequest.getCityId());
        hotel.setCityId(cityId);

        Hotel modifyHotel = hotelService.modifyAirport(hotel);
        return ResponseEntity.ok(modifyHotel);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<Hotel> deleteHotel(@PathVariable Long id) {
        boolean isDelete = hotelService.deleteHotel(id);
        return isDelete ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }

}

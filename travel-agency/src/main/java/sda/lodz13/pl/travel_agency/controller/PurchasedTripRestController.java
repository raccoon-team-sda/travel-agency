package sda.lodz13.pl.travel_agency.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.controller.model.PurchasedTripRequest;
import sda.lodz13.pl.travel_agency.model.Hotel;
import sda.lodz13.pl.travel_agency.model.PurchasedTrip;
import sda.lodz13.pl.travel_agency.model.Trip;
import sda.lodz13.pl.travel_agency.model.User;
import sda.lodz13.pl.travel_agency.service.PurchasedTripService;
import sda.lodz13.pl.travel_agency.service.TripService;
import sda.lodz13.pl.travel_agency.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/rest/purchasedTrip")
public class PurchasedTripRestController {

    private final PurchasedTripService purchasedTripService;
    private final TripService tripService;
    private final UserService userService;


    public PurchasedTripRestController(PurchasedTripService purchasedTripService,
                                       TripService tripService,
                                       UserService userService) {
        this.purchasedTripService = purchasedTripService;
        this.tripService = tripService;
        this.userService = userService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<PurchasedTrip> savePurchasedTrip(PurchasedTripRequest purchasedTripRequest) {
        PurchasedTrip purchasedTrip = new PurchasedTrip();

        purchasedTrip.setId(purchasedTripRequest.getId());

        Trip trip = tripService.getTripById(purchasedTripRequest.getTripId());
        purchasedTrip.setTripId(trip);

        purchasedTrip.setAmount(purchasedTrip.getAmount());

        User user = userService.getUserById(purchasedTripRequest.getUserId());
        purchasedTrip.setUserId(user);

        PurchasedTrip addPurchasedTrip = purchasedTripService.addPurchasedTrip(purchasedTrip);
        return ResponseEntity.ok(addPurchasedTrip);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<PurchasedTrip>> getAll() {
        List<PurchasedTrip> listAllPurchasedTrip = purchasedTripService.getListAllPurchasedTrip();
        return ResponseEntity.ok(listAllPurchasedTrip);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<PurchasedTripRequest> getPurchasedTripById(@PathVariable Long id) {
        PurchasedTripRequest purchasedTripRequest = new PurchasedTripRequest();
        PurchasedTrip purchasedTrip = purchasedTripService.getPurchasedTripById(id);

        purchasedTripRequest.setId(purchasedTrip.getId());
        purchasedTripRequest.setTripId(purchasedTrip.getTripId().getId());
        purchasedTripRequest.setAmount(purchasedTrip.getAmount());
        purchasedTripRequest.setUserId(purchasedTrip.getUserId().getId());

        try {
            return ResponseEntity.ok(purchasedTripRequest);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

//    @CrossOrigin(origins = "http://localhost:4200")
//    @PutMapping(value = "/update")
//    public ResponseEntity<PurchasedTrip>mobifyPurchasedTrip(@RequestBody PurchasedTripRequest purchasedTripRequest){
//        PurchasedTrip purchasedTrip = new PurchasedTrip();
//
//    }
}

package sda.lodz13.pl.travel_agency.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@Entity
public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String standard;
    private String description;
    @ManyToOne
    @JoinColumn(name = "city_id")
    private City cityId;
}


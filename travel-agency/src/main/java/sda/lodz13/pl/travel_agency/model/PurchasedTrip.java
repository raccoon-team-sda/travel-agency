package sda.lodz13.pl.travel_agency.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
@Getter
@Setter
@Entity
public class PurchasedTrip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "trip_id")
    private Trip tripId;
    private Integer amount;

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User userId;
}

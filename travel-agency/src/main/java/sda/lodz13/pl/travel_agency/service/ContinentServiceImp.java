package sda.lodz13.pl.travel_agency.service;

import sda.lodz13.pl.travel_agency.model.Continent;

import java.util.List;

public interface ContinentServiceImp {


    Continent addContinent(Continent continent);
//    Continent addContinent(Continent continent);

    Continent getContinentById(long id);

    List<Continent> getContinentByName(String name);

    Continent modifyContinent(Continent continent);

    boolean deleteContinent(long id);

    List<Continent> getListAllContinents();

}

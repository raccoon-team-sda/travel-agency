package sda.lodz13.pl.travel_agency.service;

import sda.lodz13.pl.travel_agency.model.City;

import java.util.List;

public interface CityServiceImp {
    City addCity(City city);

    City getCityById(long id);

    List<City> getCityByName(String name);

    City modifyCity(City continent);

    boolean deleteCity(long id);

    List<City> getListAllCities();
}

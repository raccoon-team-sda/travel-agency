package sda.lodz13.pl.travel_agency.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.controller.model.AirportRequest;
import sda.lodz13.pl.travel_agency.model.Airport;
import sda.lodz13.pl.travel_agency.model.City;
import sda.lodz13.pl.travel_agency.service.AirportService;
import sda.lodz13.pl.travel_agency.service.CityService;

import java.util.List;

@RestController
@RequestMapping("/rest/airport")
public class AirportRestController {

    private final AirportService airportService;
    private final CityService cityService;

    public AirportRestController(AirportService airportService, CityService cityService) {
        this.airportService = airportService;
        this.cityService = cityService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Airport> saveAirport(@RequestBody AirportRequest airportRequest) {
        Airport a = new Airport();

        a.setId(airportRequest.getId());
        a.setName(airportRequest.getName());

        City cityId = cityService.getCityById(airportRequest.getCityId());
        a.setCityId(cityId);

        Airport addedAirport = airportService.addAirport(a);
        return ResponseEntity.ok(addedAirport);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<Airport>> getAll() {
        List<Airport> listAllAirports = airportService.getListAllAirport();
        return ResponseEntity.ok(listAllAirports);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<AirportRequest> getAirportById(@PathVariable Long id) {
        AirportRequest airportRequest = new AirportRequest();
        Airport airport = airportService.getAirportById(id);

        airportRequest.setId(airport.getId());
        airportRequest.setName(airport.getName());
        airportRequest.setCityId(airport.getCityId().getId());

        try {
            return ResponseEntity.ok(airportRequest);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping(value = "/update")
    public ResponseEntity<Airport> modifyAirport(@RequestBody AirportRequest airportRequest) {
        Airport a = new Airport();

        a.setId(airportRequest.getId());
        a.setName(airportRequest.getName());

        City cityId = cityService.getCityById(airportRequest.getCityId());
        a.setCityId(cityId);

        Airport modifyAirport = airportService.modifyAirport(a);
        return ResponseEntity.ok(modifyAirport);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<Airport> deleteAirport(@PathVariable Long id) {
        boolean isDelete = airportService.deleteAirport(id);
        return isDelete ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }
}

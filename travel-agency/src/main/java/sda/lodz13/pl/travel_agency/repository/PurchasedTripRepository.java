package sda.lodz13.pl.travel_agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sda.lodz13.pl.travel_agency.model.PurchasedTrip;

@Repository
public interface PurchasedTripRepository extends JpaRepository<PurchasedTrip, Long> {
}

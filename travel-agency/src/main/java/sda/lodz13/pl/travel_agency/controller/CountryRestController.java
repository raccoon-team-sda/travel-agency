package sda.lodz13.pl.travel_agency.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sda.lodz13.pl.travel_agency.controller.model.CountryRequest;
import sda.lodz13.pl.travel_agency.model.Continent;
import sda.lodz13.pl.travel_agency.model.Country;
import sda.lodz13.pl.travel_agency.service.ContinentService;
import sda.lodz13.pl.travel_agency.service.CountryService;

import java.util.List;

@RestController
@RequestMapping("/rest/country")
public class CountryRestController {

    //
    @Autowired
    private final CountryService countryService;
    private final ContinentService continentService;

    public CountryRestController(CountryService countryService, ContinentService continentService) {
        this.countryService = countryService;
        this.continentService = continentService;
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    public ResponseEntity<Country> saveCountry(@RequestBody CountryRequest countryRequest) {
        Country c = new Country();

        c.setId(countryRequest.getId());
        c.setName(countryRequest.getName());

        Continent continent = continentService.getContinentById(countryRequest.getContinentId());
        c.setContinentId(continent);

        Country countryAdded = countryService.addCountry(c);
        return ResponseEntity.ok(countryAdded);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/all")
    public ResponseEntity<List<Country>> getAll() {
        List<Country> listAllCountries = countryService.getListAllCountry();
        return ResponseEntity.ok(listAllCountries);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @RequestMapping(method = RequestMethod.GET, value = "/get/{id}")
    public ResponseEntity<CountryRequest> getCountryById(@PathVariable Long id) {
        CountryRequest countryRequest = new CountryRequest();
        Country country = countryService.getCountryById(id);

        countryRequest.setId(country.getId());
        countryRequest.setName(country.getName());
        countryRequest.setContinentId(country.getContinentId().getId());

        try {
            return ResponseEntity.ok(countryRequest);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @PutMapping(value = "/update")
    public ResponseEntity<Country> modifyCountry(@RequestBody CountryRequest countryRequest) {
        Country c = new Country();
        c.setId(countryRequest.getId());
        c.setName(countryRequest.getName());

        Continent continentId = continentService.getContinentById(countryRequest.getContinentId());
        c.setContinentId(continentId);

        Country countryModify = countryService.modifyCountry(c);
        return ResponseEntity.ok(countryModify);
    }

    @CrossOrigin(origins = "http://localhost:4200")
    @DeleteMapping(value = "delete/{id}")
    public ResponseEntity<Country> deleteCountry(@PathVariable Long id) {
        boolean isDelete = countryService.deleteCountry(id);
        return isDelete ? ResponseEntity.ok().build() : ResponseEntity.badRequest().build();
    }

}

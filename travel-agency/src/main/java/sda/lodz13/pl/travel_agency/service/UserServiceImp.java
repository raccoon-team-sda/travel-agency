package sda.lodz13.pl.travel_agency.service;


import sda.lodz13.pl.travel_agency.model.User;

import java.util.List;

public interface UserServiceImp {
    User addUser(User user);

    User getUserById(long id);

    List<User> getUserByName(String name);

    User modifyUser(User user);

    boolean deleteUser(long id);

    List<User> getListAllUser();
}

package sda.lodz13.pl.travel_agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sda.lodz13.pl.travel_agency.model.Continent;
import sda.lodz13.pl.travel_agency.model.Country;

import java.util.List;

@Repository
public interface CountryRepository extends JpaRepository<Country, Long> {
    List<Country> findByNameIgnoreCaseContaining(String name);
}

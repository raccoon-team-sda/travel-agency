package sda.lodz13.pl.travel_agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.lodz13.pl.travel_agency.model.PurchasedTrip;
import sda.lodz13.pl.travel_agency.repository.PurchasedTripRepository;

import java.util.List;

@Service
public class PurchasedTripService implements PurchasedTripImp {

    @Autowired
    private PurchasedTripRepository purchasedTripRepository;

    @Override
    public PurchasedTrip addPurchasedTrip(PurchasedTrip purchasedTrip) {
        return purchasedTripRepository.save(purchasedTrip);
    }


    @Override
    public PurchasedTrip getPurchasedTripById(long id) {
        return purchasedTripRepository.findById(id).get();
    }

    @Override
    public List<PurchasedTrip> getPurchasedTripByName(String name) {
        return null;
    }

    @Override
    public PurchasedTrip modifyPurchasedTrip(PurchasedTrip purchasedTrip) {
        return purchasedTripRepository.save(purchasedTrip);
    }

    @Override
    public boolean deletePurchasedTrip(long id) {
        try {
            purchasedTripRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<PurchasedTrip> getListAllPurchasedTrip() {
        return purchasedTripRepository.findAll();
    }
}

package sda.lodz13.pl.travel_agency.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sda.lodz13.pl.travel_agency.model.Continent;

import java.util.List;

@Repository
public interface ContinentRepository extends JpaRepository<Continent, Long> {
    List<Continent> findByNameIgnoreCaseContaining(String name);
}

package sda.lodz13.pl.travel_agency.service;

import sda.lodz13.pl.travel_agency.model.Hotel;

import java.util.List;

public interface HotelServiceImp {
    List<Hotel> getListAllHotel();

    Hotel addHotel(Hotel airport);

    Hotel getHotelById(long id);

    List<Hotel> getHotelByName(String name);

    Hotel modifyAirport(Hotel hotel);

    boolean deleteHotel(long id);
}

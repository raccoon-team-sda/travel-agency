package sda.lodz13.pl.travel_agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.lodz13.pl.travel_agency.model.Continent;
import sda.lodz13.pl.travel_agency.repository.ContinentRepository;

import java.util.List;

@Service
public class ContinentService implements ContinentServiceImp {

    @Autowired
    private ContinentRepository continentRepository;

    @Override
    public Continent addContinent(Continent continent) {
        return continentRepository.save(continent);
    }

    @Override
    public Continent getContinentById(long id) {
        return continentRepository.findById(id).get();
    }

    @Override
    public List<Continent> getContinentByName(String name) {
        return continentRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public Continent modifyContinent(Continent continent) {
        return continentRepository.save(continent);
    }

    @Override
    public boolean deleteContinent(long id) {
        try {
            continentRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<Continent> getListAllContinents() {
        return continentRepository.findAll();
    }
}

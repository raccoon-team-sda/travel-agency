package sda.lodz13.pl.travel_agency.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sda.lodz13.pl.travel_agency.model.City;
import sda.lodz13.pl.travel_agency.repository.CityRepository;

import java.util.List;

@Service
public class CityService implements CityServiceImp {

    @Autowired
    private CityRepository cityRepository;

    @Override
    public City addCity(City city) {
        return cityRepository.save(city);
    }

    @Override
    public City getCityById(long id) {
        return cityRepository.findById(id).get();
    }

    @Override
    public List<City> getCityByName(String name) {
        return cityRepository.findByNameIgnoreCaseContaining(name);
    }

    @Override
    public City modifyCity(City city) {
        return cityRepository.save(city);
    }

    @Override
    public boolean deleteCity(long id) {
        try {
            cityRepository.deleteById(id);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public List<City> getListAllCities() {
        return cityRepository.findAll();
    }
}

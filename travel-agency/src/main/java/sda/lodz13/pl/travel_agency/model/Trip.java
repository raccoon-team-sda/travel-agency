package sda.lodz13.pl.travel_agency.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "airport_from")
    private Airport airportFrom;

    @ManyToOne
    @JoinColumn(name = "airport_to")
    private Airport airportTo;

    @ManyToOne
    @JoinColumn(name = "hotel_to")
    private Hotel hotelTo;

    @ManyToOne
    @JoinColumn(name = "city_to")
    private City cityTo;

    private int departureDate;
    private int returnDate;
    private int countOfDays;
    private String type;
    private double priceForAdult;
    private double priceForChild;
    private String promotion;
    private int countOfPerson;
    private String description;
}

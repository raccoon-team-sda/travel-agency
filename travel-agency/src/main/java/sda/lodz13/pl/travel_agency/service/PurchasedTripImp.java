package sda.lodz13.pl.travel_agency.service;

import sda.lodz13.pl.travel_agency.model.PurchasedTrip;

import java.util.List;

public interface PurchasedTripImp {
    PurchasedTrip addPurchasedTrip(PurchasedTrip purchasedTrip);

    PurchasedTrip getPurchasedTripById(long id);

    List<PurchasedTrip> getPurchasedTripByName(String name);

    PurchasedTrip modifyPurchasedTrip(PurchasedTrip purchasedTrip);

    boolean deletePurchasedTrip(long id);

    List<PurchasedTrip> getListAllPurchasedTrip();

}

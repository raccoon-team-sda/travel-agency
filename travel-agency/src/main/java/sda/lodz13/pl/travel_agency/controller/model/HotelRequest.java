package sda.lodz13.pl.travel_agency.controller.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRequest {
    Long id;
    String name;
    String standard;
    String description;
    long cityId;
}

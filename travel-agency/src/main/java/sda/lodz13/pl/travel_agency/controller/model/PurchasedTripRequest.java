package sda.lodz13.pl.travel_agency.controller.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PurchasedTripRequest {
    Long id;
    long tripId;
    int amount;
    long userId;
}

package sda.lodz13.pl.travel_agency.model;

import javax.persistence.*;

@Entity
public class Hotel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String standard;
    private String description;
    @ManyToOne
    @JoinColumn(name = "city_id")
    private City cityId;


}


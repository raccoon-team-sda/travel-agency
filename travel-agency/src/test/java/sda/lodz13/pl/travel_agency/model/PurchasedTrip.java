package sda.lodz13.pl.travel_agency.model;

import javax.persistence.*;

@Entity
public class PurchasedTrip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @OneToMany
    @JoinColumn(name = "trip_id")
    private Trip tripId;
    private Integer amount;
    private String owner;
}

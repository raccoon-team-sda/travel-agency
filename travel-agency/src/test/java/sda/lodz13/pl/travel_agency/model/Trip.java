package sda.lodz13.pl.travel_agency.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Trip {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Airport airportFrom;
    private Airport airportTo;
    private Hotel hotelTo;
    private City cityTo;
    private Date departureDate;
    private Date returnDate;
    private int countOfDays;
    private String type;
    private double priceForAdult;
    private double priceForChild;
    private String promotion;
    private int countOfPerson;
    private String description;


}

package sda.lodz13.pl.travel_agency.model;

import javax.persistence.*;

@Entity
public class Continent {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
}
